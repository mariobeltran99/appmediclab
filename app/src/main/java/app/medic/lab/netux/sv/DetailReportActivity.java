package app.medic.lab.netux.sv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class DetailReportActivity extends AppCompatActivity {

    List<Product> list;
    RecyclerView recyclerViewProductsDetail;
    TextView tvAmountCar;
    CarShopAdapter cAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_report_layout);
        list = (List<Product>) getIntent().getSerializableExtra("products");
        loadView();
        recyclerViewProductsDetail.setLayoutManager(new LinearLayoutManager(this));
        cAdapter = new CarShopAdapter(DetailReportActivity.this,list,tvAmountCar);
        recyclerViewProductsDetail.setAdapter(cAdapter);
    }
    private void loadView(){
        recyclerViewProductsDetail = findViewById(R.id.listProductsDetails);
        tvAmountCar = findViewById(R.id.tvAmountProductDetail);
    }
}