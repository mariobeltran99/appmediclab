package app.medic.lab.netux.sv;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarShopActivity extends AppCompatActivity {

    List<Product> carShop;
    CarShopAdapter cAdapter;
    RecyclerView listCarShop;
    TextView tvAmount;
    Button btnBuy;
    FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_shop_layout);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();
        carShop = (List<Product>) getIntent().getSerializableExtra("carShop");
        loadView();
        listCarShop.setLayoutManager(new LinearLayoutManager(this));
        cAdapter = new CarShopAdapter(CarShopActivity.this, carShop, tvAmount);
        listCarShop.setAdapter(cAdapter);
        btnBuy.setOnClickListener(v -> {
            double prices = 0;
            prices = amountProducts(carShop);
            AlertDialog.Builder builder = new AlertDialog.Builder(CarShopActivity.this);
            if(prices == 0.0){
                builder.setIcon(R.drawable.ic_baseline_report_problem_24)
                        .setTitle("Advertencia de Pago")
                        .setMessage("No hay productos en su carro de compras.")
                        .setPositiveButton("Aceptar", (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .show();
            }else{
                builder.setIcon(R.drawable.ic_baseline_payment_24)
                        .setTitle("Confirmar Pago")
                        .setMessage("¿Deseas pagar estos productos a tu cuenta?")
                        .setPositiveButton("Aceptar", (dialog, which) -> {
                            buyAndRegister(user,carShop);
                        })
                        .setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss())
                        .show();
            }
        });
    }
    private void loadView(){
        listCarShop = findViewById(R.id.listCarShop);
        tvAmount = findViewById(R.id.tvAmountProduct);
        btnBuy = findViewById(R.id.btnBuy);
    }
    private double amountProducts(List<Product> list){
        double amount = 0;
        DecimalFormat ds = new DecimalFormat("0.00");
        for (int i = 0; i < list.size(); i++){
            amount += Double.parseDouble(""+ list.get(i).getPrice());
        }
        return Double.parseDouble(ds.format(amount));
    }
    private void buyAndRegister(FirebaseUser user, List<Product> listCar){
        String strDateFormat = "dd/MMM/yyyy hh:mm:ss a";
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
        Date objdate = new Date();
        Map<String, Object> map = new HashMap<>();
        map.put("email", user.getEmail());
        map.put("products", listCar);
        map.put("amount", amountProducts(listCar));
        map.put("created", objSDF.format(objdate));
        mFirestore.collection("Reports")
                .add(map)
                .addOnSuccessListener(documentReference -> {
                    Toast.makeText(CarShopActivity.this,"La compra se registró exitosamente.", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(CarShopActivity.this,DashboardActivity.class);
                    startActivity(i);
                    finish();
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(CarShopActivity.this,"La compra no se puedo realizar en este momento.",Toast.LENGTH_LONG).show();
                });
    }
}