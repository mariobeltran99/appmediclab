package app.medic.lab.netux.sv;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    ImageButton logout, shop, reports;
    RecyclerView recyclerViewProducts;
    ProductAdapter pAdapter;
    FirebaseFirestore mFirestore;
    List<Product> carShop = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout);
        loadView();
        mFirestore = FirebaseFirestore.getInstance();
        Query query = mFirestore.collection("Products");
        FirestoreRecyclerOptions<Product> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<Product>().setQuery(query, Product.class).build();
        pAdapter = new ProductAdapter(firestoreRecyclerOptions, carShop, shop,DashboardActivity.this);
        pAdapter.notifyDataSetChanged();
        recyclerViewProducts.setAdapter(pAdapter);
        logout.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
            builder.setIcon(R.drawable.ic_baseline_error_24)
                    .setTitle("Cerrar Sesión")
                    .setMessage("¿Estás seguro de cerrar tu sesión actual?")
                    .setPositiveButton("Aceptar", (dialog, which) -> {
                        FirebaseAuth.getInstance().signOut();
                        Intent i = new Intent(DashboardActivity.this,LoginActivity.class);
                        startActivity(i);
                        finish();
                    })
                    .setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss()).show();
        });
        reports.setOnClickListener(v -> {
            Intent i = new Intent(DashboardActivity.this, ReportActivity.class);
            startActivity(i);
        });
    }
    private void loadView(){
        logout = findViewById(R.id.btnSignOut);
        shop = findViewById(R.id.btnShop);
        reports = findViewById(R.id.btnReports);
        recyclerViewProducts = findViewById(R.id.listProducts);
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        pAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        pAdapter.stopListening();
    }
}