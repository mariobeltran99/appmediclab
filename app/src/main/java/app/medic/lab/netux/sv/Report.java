package app.medic.lab.netux.sv;

import java.io.Serializable;
import java.util.List;

public class Report implements Serializable {
    private String email;
    private List<Product> products;
    private String created;
    private double amount;

    public Report(){

    }
    public Report(String email, List<Product> products, String created, double amount){
        this.email = email;
        this.products = products;
        this.created = created;
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
