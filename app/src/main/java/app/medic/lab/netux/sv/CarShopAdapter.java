package app.medic.lab.netux.sv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class CarShopAdapter extends RecyclerView.Adapter<CarShopAdapter.ProductsViewHolder> {
    Context context;
    List<Product> listProductsCars;
    TextView tvAmount;
    double amount = 0;
    public CarShopAdapter(Context context, List<Product> listShop, TextView tvAmount){
        this.context = context;
        this.listProductsCars = listShop;
        this.tvAmount = tvAmount;
        DecimalFormat ds = new DecimalFormat("0.00");
        for (int i=0; i < listProductsCars.size(); i++){
            amount += Double.parseDouble("" + listProductsCars.get(i).getPrice());
        }
        tvAmount.setText("Total a Pagar: $"+ ds.format(amount));
    }

    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_product_carshop_layout, null, false);
        return new ProductsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CarShopAdapter.ProductsViewHolder holder, int position) {
        DecimalFormat ds = new DecimalFormat("0.00");
        holder.tvName.setText(listProductsCars.get(position).getName());
        holder.tvDescription.setText(listProductsCars.get(position).getDescription());
        holder.tvPrice.setText("$" + ds.format(listProductsCars.get(position).getPrice()));
        Picasso.get().load(listProductsCars.get(position).getPhoto()).into(holder.photoC);
    }

    @Override
    public int getItemCount() {
        return listProductsCars.size();
    }

    public class ProductsViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvDescription, tvPrice;
        ImageView photoC;
        public ProductsViewHolder(View v) {
            super(v);
            tvName = v.findViewById(R.id.tvTitleProductS);
            tvDescription = v.findViewById(R.id.tvDescriptionProductS);
            tvPrice = v.findViewById(R.id.tvPriceProductS);
            photoC = v.findViewById(R.id.imageProductS);
        }
    }
}
