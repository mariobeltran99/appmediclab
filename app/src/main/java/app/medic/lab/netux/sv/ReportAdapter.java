package app.medic.lab.netux.sv;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

public class ReportAdapter extends FirestoreRecyclerAdapter<Report, ReportAdapter.ViewHolder> {

    List<Report> listReports;
    Context context;
    public ReportAdapter(@NonNull FirestoreRecyclerOptions<Report> options, Context context) {
        super(options);
        this.listReports = options.getSnapshots();
        this.context = context;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onBindViewHolder(@NonNull ReportAdapter.ViewHolder holder, int position, @NonNull Report report) {
        DecimalFormat ds = new DecimalFormat("0.00");
        holder.datesF.setText("Fecha de Pago: " + report.getCreated());
        holder.amountP.setText("Monto de la Compra: $" + ds.format(report.getAmount()));
        holder.btnShowProducts.setOnClickListener(v -> {
            Intent i = new Intent(context,DetailReportActivity.class);
            i.putExtra("products", (Serializable) listReports.get(position).getProducts());
            i.putExtra("dates", report.getCreated());
            context.startActivity(i);
        });
    }

    @NonNull
    @Override
    public ReportAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_reports_layout, parent, false);
        return new ReportAdapter.ViewHolder(view);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView datesF, amountP;
        Button btnShowProducts;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            datesF = itemView.findViewById(R.id.tvDatePurchased);
            btnShowProducts = itemView.findViewById(R.id.btnShowProducts);
            amountP = itemView.findViewById(R.id.tvAmountSold);
        }
    }
}
