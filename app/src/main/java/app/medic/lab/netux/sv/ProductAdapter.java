package app.medic.lab.netux.sv;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

public class ProductAdapter extends FirestoreRecyclerAdapter<Product, ProductAdapter.ViewHolder> {

    List<Product> listShop;
    List<Product> listProduct;
    ImageButton btnShop;
    Context context;
    public ProductAdapter(@NonNull FirestoreRecyclerOptions<Product> options, List<Product> listCarShop, ImageButton btnCarShp, Context context) {
        super(options);
        this.btnShop = btnCarShp;
        this.listShop = listCarShop;
        this.listProduct = options.getSnapshots();
        this.context = context;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Product product) {
        DecimalFormat ds = new DecimalFormat("0.00");
        holder.titleP.setText(product.getName());
        holder.descriptionP.setText(product.getDescription());
        holder.priceP.setText("$" + ds.format(product.getPrice()));
        Picasso.get().load(product.getPhoto()).into(holder.photoP);
        holder.chkProductCart.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(holder.chkProductCart.isChecked()){
                listShop.add(listProduct.get(position));
                Toast.makeText(context,"Se ha agregado al carro de compras",Toast.LENGTH_LONG).show();
            }else{
                listShop.remove(listProduct.get(position));
                Toast.makeText(context,"Se ha eliminado del carro de compras",Toast.LENGTH_LONG).show();
            }
        });
        btnShop.setOnClickListener(v -> {
            Intent i = new Intent(context,CarShopActivity.class);
            i.putExtra("carShop", (Serializable) listShop);
            context.startActivity(i);
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_products_layout, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleP, descriptionP, priceP;
        ImageView photoP;
        CheckBox chkProductCart;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleP = itemView.findViewById(R.id.tvTitleProduct);
            descriptionP = itemView.findViewById(R.id.tvDescriptionProduct);
            priceP = itemView.findViewById(R.id.tvPriceProduct);
            photoP = itemView.findViewById(R.id.imageProduct);
            chkProductCart = itemView.findViewById(R.id.chkProduct);
        }
    }
}
