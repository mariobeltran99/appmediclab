package app.medic.lab.netux.sv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

public class ReportActivity extends AppCompatActivity {

    ImageView imageUser;
    TextView tvUserName;
    ReportAdapter rAdapter;
    RecyclerView recyclerViewReports;
    FirebaseFirestore mFirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_layout);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();
        loadView();
        Picasso.get().load(user.getPhotoUrl()).into(imageUser);
        tvUserName.setText(user.getDisplayName());
        Query query = mFirestore.collection("Reports").whereEqualTo("email", user.getEmail());
        FirestoreRecyclerOptions<Report> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<Report>().setQuery(query, Report.class).build();
        rAdapter = new ReportAdapter(firestoreRecyclerOptions,ReportActivity.this);
        rAdapter.notifyDataSetChanged();
        recyclerViewReports.setAdapter(rAdapter);
    }
    private void loadView(){
        imageUser = findViewById(R.id.imageUser);
        tvUserName = findViewById(R.id.tvNameUser);
        recyclerViewReports = findViewById(R.id.listReports);
        recyclerViewReports.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    protected void onStart() {
        super.onStart();
        rAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        rAdapter.stopListening();
    }
}