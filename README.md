# MedicLab #
![AppLogo](app/src/main/res/mipmap-xxhdpi/ic_launcher_round.png)


Es una aplicación desarrollada en Android Studio, utilizando como lenguaje de programación principal JAVA, la aplicación ayuda con el manejo de una tienda en línea con productos como medicamentos para la empresa MedicLab, donde proporciona al usuario una vista de carrito de compras y ver sus últimas compras realizadas junto con los detalles de los productos que se tienen a la venta.

# Dependencias #
------
```
dependencies {
    ...
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'com.google.android.material:material:1.3.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'androidx.navigation:navigation-fragment:2.2.2'
    implementation 'androidx.navigation:navigation-ui:2.2.2'
    implementation 'androidx.lifecycle:lifecycle-livedata-ktx:2.3.0'
    implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.0'
    implementation 'com.google.firebase:firebase-auth:20.0.3'
    implementation 'com.google.android.gms:play-services-auth:19.0.0'
    implementation 'com.google.firebase:firebase-firestore:22.1.2'
    implementation 'com.firebaseui:firebase-ui-firestore:7.1.1'
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    implementation 'com.squareup.picasso:picasso:2.71828'
}
```

# Guía de Usuario #

En el siguiente enlace está la documentación del funcionamiento de la aplicación y el manejo de la misma para los usuarios. [Documentación de MedicLab](https://bitbucket.org/mariobeltran99/appmediclab/src/master/Documentos/Manual%20de%20usuario%20APPSMediclab.pdf).

# Mockups #

Los siguientes diseños se crearon en la plataforma de [Figma](https://www.figma.com/file/eURwbHr8J9LBwV1SiYCiWD/Mockups-de-MedicLab?node-id=8%3A101).

# Autores #

* Mario Josué Beltrán García BG171969  L03  
* Andrés Eduardo Molina Moz MM161405 L03

# Licencias #

-------

    Copyright 2014 - 2020 Henning Dodenhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

